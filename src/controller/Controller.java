package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.naming.spi.DirStateFactory.Result;

import org.w3c.dom.ls.LSInput;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import test_tables.LinearPSeparateCTest;
import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinearProbingHashST;
import model.data_structures.Queue;
import model.data_structures.SeparateChainingHashST;
import model.data_structures.Stack;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;
import model.util.*;

public class Controller {

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> QueueCopy;

	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;

	private LinearProbingHashST<Integer, IQueue<VOMovingViolations>> linearHash;

	private SeparateChainingHashST<Integer, IQueue<VOMovingViolations>> separateHash;

	public Controller() {
		view = new MovingViolationsManagerView();

		// TODO, inicializar la pila y la cola
		movingViolationsQueue = new Queue<VOMovingViolations>();
		movingViolationsStack = new Stack<VOMovingViolations>();
		linearHash = new LinearProbingHashST<>();
		separateHash = new SeparateChainingHashST<>();
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean loaded = false;
		boolean fin = false;

		while (!fin) {
			view.printMenu();

			int option = sc.nextInt();

			switch (option) {
			case 1: // CARGAR
				if (!loaded) {
					this.ReadJSONExample();
					loaded = true;
					break;
				} 

				else 
				{
					System.out.println();
					System.out.println("Los datos ya fueron cargados (Solo se pueden cargar una vez)");
					System.out.println();
					break;
				}

			case 2:

				if (loaded)
				{
					System.out.println("Ingrese la direccion de la calle a buscar");
					int addressId = sc.nextInt();

					accidentesPordireccionLineal(addressId);
				}
				else
				{
					System.out.println();
					System.out.println("Los datos no han sido cargados");
					System.out.println();
				}

				break;

			case 3:

				if (loaded)
				{
					System.out.println("Ingrese la direccion de la calle a buscar");
					int addressId2 = sc.nextInt();

					accidentesPorDireccionSeparate(addressId2);
				}
				else
				{
					System.out.println();
					System.out.println("Los datos no han sido cargados");
					System.out.println();
				}
				break;


			case 4:
				if (loaded)
				{
					System.out.println();
					LinearPSeparateCTest.setUpEscenario1();
					System.out.println();
				}
				else
				{
					System.out.println();
					System.out.println("Los datos no han sido cargados");
					System.out.println();
				}
				break;

			case 0:
				fin = true;
				sc.close();
				break;
			}
		}
	}

	public void ReadJSONExample() {

		for (int i = 1; i <= 6; i++) {

			File file = new File("data/Moving_Violations_Issued_in_Month_" + i + "_2018.json");

			// JSON parser object to parse read file
			JsonParser jsonParser = new JsonParser();

			try (FileReader reader = new FileReader(file)) 
			{
				BufferedReader br = new BufferedReader(new FileReader(file));

				br.readLine();
				String line = br.readLine();
				String txt = "";
				while (line != null) 
				{
					txt = txt + line;

					if (line.contains("}") || line.contains("},")) 
					{

						if (txt.endsWith(",")) {
							txt = txt.substring(0, txt.length() - 1);
						}
						com.google.gson.JsonObject obj = (JsonObject) jsonParser.parse(txt);
						obj.get("OBJECTID");
						loadObject(obj);
						txt = "";

					}
					line = br.readLine();
				}



			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println();
		System.out.println("Los datos fueron cargados");
		System.out.println();

	}

	public void loadObject(com.google.gson.JsonObject o) {
		int objectId = o.get("OBJECTID").getAsInt();
		String location = o.get("LOCATION").getAsString();
		int adressID = -1;
		double streetSegID = -1;
		try {
			adressID = o.get("ADDRESS_ID").getAsInt();
			streetSegID = o.get("STREETSEGID").getAsDouble();
		} catch (Exception e) {
		}
		double xCord = o.get("XCOORD").getAsDouble();
		double yCord = o.get("YCOORD").getAsDouble();
		String ticketType = o.get("TICKETTYPE").getAsString();
		int fineamt = o.get("FINEAMT").getAsInt();
		double totalPaid = o.get("TOTALPAID").getAsDouble();
		int penalty1 = o.get("PENALTY1").getAsInt();
		String accidentIndicator = o.get("ACCIDENTINDICATOR").getAsString();
		String ticketIssueDate = o.get("TICKETISSUEDATE").getAsString();
		String violationCode = o.get("VIOLATIONCODE").getAsString();
		String violationDesc = o.get("VIOLATIONDESC").getAsString();

		VOMovingViolations obj = new VOMovingViolations(objectId, location, adressID, streetSegID, xCord, yCord, ticketType, fineamt, totalPaid, penalty1, accidentIndicator, ticketIssueDate, violationCode, violationDesc);

		if (linearHash.contains(obj.getAdress())) {
			IQueue<VOMovingViolations> lista = linearHash.get(obj.getAdress());
			lista.enqueue(obj);
		}
		else {
			IQueue<VOMovingViolations> lista  = new Queue<VOMovingViolations>();
			lista.enqueue(obj);
			linearHash.put(obj.getAdress(), lista);
		}
		if (separateHash.contains(obj.getAdress())) {
			IQueue<VOMovingViolations> lista = separateHash.get(obj.getAdress());
			lista.enqueue(obj);
		}
		else {
			IQueue<VOMovingViolations> lista  = new Queue<VOMovingViolations>();
			lista.enqueue(obj);
			separateHash.put(obj.getAdress(), lista);

		}
	}


	public void accidentesPordireccionLineal(int key) {
		int count =0; 
		IQueue<VOMovingViolations> list = linearHash.get(key);

		if (list != null)
		{
			VOMovingViolations arr[] = new VOMovingViolations[list.size()];
			int i = 0;
			for (VOMovingViolations voMovingViolations : list) {
				arr[i++] = voMovingViolations;
			}
			Sort.ordenarQuickSort(arr);	

			for (VOMovingViolations voMovingViolations : arr) 
			{
				if (voMovingViolations.getAccidentIndicator().equals("Yes")) 
				{
					int objId = voMovingViolations.getobjectId();
					String  location = voMovingViolations.getLocation();
					String ticketissuedate = voMovingViolations.getTicketIssueDate();
					String violationCode = voMovingViolations.getViolationCode();
					int finemat = voMovingViolations.getFineAmt();
					System.out.println("Id:" +objId+" Lugar: "+location+" Fecha: "+ticketissuedate+" Codigo de violacion: "+violationCode+" Multa: "+finemat);
					count++;
				}
			}
			System.out.println("Numero accidentes = "+count);
		}
		else
		{
			System.out.println();
			System.out.println("No se encontraron accidentes en la direcci�n dada");
			System.out.println();
		}
	}

	public void accidentesPorDireccionSeparate(int key) 
	{
		IQueue<VOMovingViolations> list = separateHash.get(key);

		if (list != null)
		{
			VOMovingViolations arr[] = new VOMovingViolations[list.size()];
			int i = 0 ;
			for (VOMovingViolations voMovingViolations : list) {

				arr[i++] = voMovingViolations;
			}
			Sort.ordenarQuickSort(arr);	

			for (VOMovingViolations voMovingViolations : arr) 
			{
				if (voMovingViolations.getAccidentIndicator().equals("Yes")) 
				{
					int objId = voMovingViolations.getobjectId();
					String  location = voMovingViolations.getLocation();
					String ticketissuedate = voMovingViolations.getTicketIssueDate();
					String violationCode = voMovingViolations.getViolationCode();
					int finemat = voMovingViolations.getFineAmt();
					System.out.println("Id:" +objId+" Lugar: "+location+" Fecha: "+ticketissuedate+" Codigo de violacion: "+violationCode+" Multa: "+finemat);
				}
			}
		}
		else
		{
			System.out.println();
			System.out.println("No se encontraron accidentes en la direcci�n dada");
			System.out.println();
		}
	}
}
