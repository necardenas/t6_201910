package test_tables;

import controller.Controller;
import model.data_structures.IQueue;
import model.data_structures.LinearProbingHashST;
import model.data_structures.SeparateChainingHashST;
import model.vo.VOMovingViolations;


public class LinearPSeparateCTest {


	public static void main(String[] args) 
	{
		LinearPSeparateCTest hola = new LinearPSeparateCTest();
		hola.setUpEscenario1();


	}

	public static void setUpEscenario1() {


		LinearProbingHashST<Integer, Integer> linearHash = new LinearProbingHashST<Integer, Integer>();

		double keyValuePairsLinear;

		double initialSizeLinerahash = linearHash.SeparateChainingHashSTSize();

		double finalSizeLinearHash;

		double finalChargeFactorLinearHash;

		double resizeLinear;

		double timeLinear;

		SeparateChainingHashST<Integer, Integer> separateHash = new SeparateChainingHashST<Integer, Integer>();

		double keyValuePairsSeparate;

		double initialSizeSeparateHash = separateHash.SeparateChainingHashSTSize();	

		double finalSizeSeparateHash;

		double finalChargeFactorSeparateHash;

		double resizeSeparate;

		double timeSeparate;


		int[] list = new int[1000000];
		for (int i = 0; i < list.length; i++) {
			list[i] = i;
			linearHash.put(i, i);
			separateHash.put(i, i);
		}

		/**
		 * Prueba para la LinearProbingHash
		 */
		double startTimeLinear = System.currentTimeMillis();
		for (int i = 0; i < 100000; i++) {
			linearHash.get(i*10);

		}
		double endTimeLinear = System.currentTimeMillis();
		keyValuePairsLinear = linearHash.size();
		finalSizeLinearHash = linearHash.SeparateChainingHashSTSize();
		finalChargeFactorLinearHash  = keyValuePairsLinear/finalSizeLinearHash;
		timeLinear = (endTimeLinear - startTimeLinear)/1000000;
		resizeLinear =  linearHash.rehash();



		/**
		 * Prueba para la SeparateChainingHash
		 */
		double startTimeSeparte = System.currentTimeMillis();
		for (int i = 0; i < 100000; i++) {
			separateHash.get(i*10);
		}

		double endTimeSeparte = System.currentTimeMillis();
		keyValuePairsSeparate = separateHash.size();
		finalSizeSeparateHash = separateHash.SeparateChainingHashSTSize();
		finalChargeFactorSeparateHash  = keyValuePairsSeparate/finalSizeSeparateHash;
		timeSeparate = (endTimeSeparte - startTimeSeparte)/1000000;
		resizeSeparate = separateHash.rehash();
		System.out.println(""+startTimeSeparte+"    "+endTimeSeparte);


		System.out.println("       					      "+"Tabla de Hash Linear Probing  |  Tabla de Hash Separate Chaining");
		System.out.println("N�mero de duplas (K,V) en la tabla"+"            "+keyValuePairsLinear+"        	 	       "+keyValuePairsSeparate);
		System.out.println("Tama�o inicial del arreglo de la tabla"+"        "+initialSizeLinerahash+"  		 	       "+initialSizeSeparateHash);
		System.out.println("Tama�o final del arreglo de la tabla"+"          "+finalSizeLinearHash+"        	 	       "+finalSizeSeparateHash);
		System.out.println("Factor de carga final (n/m)"+"                   "+finalChargeFactorLinearHash+"               "+finalChargeFactorSeparateHash);
		System.out.println("Numero de rehashes que tuvo la tabla"+"          "+resizeLinear+"               	       "+resizeSeparate);
		System.out.println("Tiempo promedio de consultas get (milis)"+"      "+timeLinear+"                	       "+timeSeparate);

	}




}
