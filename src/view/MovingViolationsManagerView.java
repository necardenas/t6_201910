package view;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() 
	{

	}

	public void printMenu() 
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		//LOAD
		System.out.println("1. Cargar datos");
		//LOAD
		System.out.println("2. Infracciones	con	accidente por ADDRESS_ID	(Tabla	de	Hash	Linear	Probing)");
		System.out.println("3. Infracciones	con	accidente por ADDRESS_ID	(Tabla	de	Hash	Separate	Chaining)");
		System.out.println("4. Crear tabla comparativa");
		//EXIT
		System.out.println("0. Salir");
		//EXIT
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}



	public void printMovingViolations(IQueue<VOMovingViolations> violations) 
	{
		System.out.println("Se encontraron "+ violations.size() + " elementos");
		for (VOMovingViolations violation : violations) 
		{
			System.out.println(violation.getobjectId() + " " 
					+ violation.getTicketIssueDate() + " " 
					+ violation.getLocation()+ " " 
					+ violation.getViolationDescription());
		}
	}

	public void printMensage(String mensaje) 
	{
		System.out.println(mensaje);
	}
}
